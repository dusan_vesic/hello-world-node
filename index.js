const express = require('express')
const app = express()
const mysql = require('mysql');

const connection = mysql.createConnection({
  host     : '192.168.0.40',
  user     : 'root',
  database : 'fake'
});

connection.connect();

app.get('/', (req, res) => {
  connection.query('SELECT * from books', (error, results, fields) => {
    if (error) throw error;
    res.send(results);
    connection.end();
  });
});

app.listen(3000, () => console.log('Example app listening on port 3000!'))